const fs = require('fs');
const path = require('path');

const SUCCESS_MESSAGE = 'Success';

function correctFilename(filename) {
    const extensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

    const splittedFilename = filename.split('.');
    if (!splittedFilename.length) return false;

    const fileExtension = splittedFilename[splittedFilename.length - 1];
    return extensions.includes(fileExtension);
}

function createFile(req, res, next) {
    const {filename, content} = req.body;

    if (!content) {
        return res.status(400).json({message: "Please specify 'content' parameter"})
    }
    if (!filename) {
        return res.status(400).json({message: "Please specify 'filename' parameter"})
    }
    if (!correctFilename(filename)) {
        return res.status(400).json({message: "Enter the correct filename"})
    }
    if (fs.existsSync(path.resolve(__dirname, 'files', filename))) {
        return res.status(400).json({message: `There is already file with name: ${filename}`})
    }

    fs.writeFileSync(path.resolve(__dirname, 'files', filename), content);
    return res.json({message: "File created successfully"});
}

function getFiles(req, res, next) {
    const files = fs.readdirSync(path.resolve(__dirname, 'files'), {withFileTypes: true})
        .filter(item => !item.isDirectory())
        .map(item => item.name);

    return res.json({
        message: SUCCESS_MESSAGE,
        files: files
    });
}

const getFile = (req, res, next) => {
    const {filename} = req.params;
    const filepath = path.resolve(__dirname, 'files', filename);

    if (!fs.existsSync(filepath)) {
        return res.status(400).json({message: `No file with '${filename}' filename found`})
    }

    const data = fs.readFileSync(filepath, {encoding: 'utf-8'});

    return res.json({
        message: SUCCESS_MESSAGE,
        filename: filename,
        content: data,
        extension: path.extname(filename).replace('.', ''),
        uploadedDate: fs.statSync(filepath).mtime
    });
}

// Other functions - editFile, deleteFile
const editFile = (req, res, next) => {
    const {filename} = req.params;
    const {content} = req.body;

    if (!content) {
        return res.status(400).json({message: "Please specify 'content' parameter"})
    }
    if (!fs.existsSync(path.resolve(__dirname, 'files', filename))) {
        return res.status(400).json({message: `No file with '${filename}' filename found`})
    }

    fs.appendFileSync(path.resolve(__dirname, 'files', filename), content)
    return res.json({message: SUCCESS_MESSAGE})
}

const deleteFile = (req, res, next) => {
    const {filename} = req.params

    if (!fs.existsSync(path.resolve(__dirname, 'files', filename))) {
        return res.status(400).json({message: `No file with '${filename}' filename found`})
    }

    fs.rmSync(path.resolve(__dirname, 'files', filename));
    return res.json({message: SUCCESS_MESSAGE})
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    editFile,
    deleteFile
}